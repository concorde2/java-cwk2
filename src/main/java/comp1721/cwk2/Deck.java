package comp1721.cwk2;

import java.util.Collections;

/**
 * @author Tang jiachen
 */
public class Deck extends CardCollection {


    /**
     * creates a deck containing the standard 52 playing cards,
     * arranged by suits and then in rank order.
     *
     */
    public Deck() {
        super();
        for (Card.Suit suit: Card.Suit.values()) {
            for (Card.Rank card: Card.Rank.values()) {
                // 'A'-> "A"  'C'-> "C" ----> "AC"
                String cardName = Character.toString(card.getSymbol()) + Character.toString(suit.getSymbol());
                cards.add(new Card(cardName));
            }
        }
//        for (Card card : cards) {
//            System.out.println(card.toString());
//            System.out.println("card rank is:--" + card.getRank().name());
//            System.out.println("card suit is:--" + card.getSuit().name());
//            System.out.println("\n");
//        }
    }

    /**
     * rearranges cards in the deck randomly.
     */
    public void shuffle() {
        Collections.shuffle(cards);
    }

    /**
     *  removes the first card in the deck and returns it.
     *
     * @return comp1721.cwk2.Card
     */
    public Card deal() {
        if (cards.isEmpty()) {
            throw new CardException("Deck is empty");
        }
        Card card = cards.get(0);
        cards.remove(0);
        return card;
    }

    /**
     * Provides the number of cards in this collection.
     *
     * @return Number of cards
     */
    @Override
    public int size() {
        return cards.size();
    }

    /**
     * Discards all the cards from this collection.
     */
    @Override
    public void discard() {
        cards.clear();
    }

    /**
     * Indicates whether this collection is empty or not.
     *
     * @return true if collection is empty, false otherwise
     */
    @Override
    public boolean isEmpty() {
        return cards.isEmpty();
    }

    /**
     * Indicates whether a particular card is present in this collection.
     *
     * @param card Card we are looking for
     * @return true if the card is present, false otherwise
     */
    @Override
    public boolean contains(Card card) {
        return cards.contains(card);
    }

    /**
     * Adds the given card to this collection.
     *
     * @param card Card to be added
     */
    @Override
    public void add(Card card) {
        cards.add(card);
    }

}
