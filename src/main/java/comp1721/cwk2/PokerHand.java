package comp1721.cwk2;

import java.util.*;

/**
 * @author Tang jiachen
 */
public class PokerHand {

    private List<Card> hands;

    /**
     *  creates an empty hand
     */
    public PokerHand() {
        hands = new LinkedList<>();
    }

    /**
     * @param s
     *  creates an hand with a String parameter
     *  that specifies the cards that should be added to the hand
     */
    public PokerHand(String s) {
        hands = new LinkedList<>();
        List<String> tempHand = Arrays.asList(s.split(" "));
        if (tempHand.size()>5) {
            throw new CardException("Too many cards");
        }
        for (String card:tempHand) {
            hands.add(new Card(card));
        }
//        for (Card card:hands) {
//            System.out.println(card.toString());
//            System.out.println("card rank is:--" + card.getRank().getSymbol());
//            System.out.println("card suit is:--" + card.getSuit().getSymbol());
//            System.out.println("\n");
//        }
    }

    /**
     * add a card into hands
     * @param card 1
     */
    public void add(Card card) {
        if (hands.size()>4) {
            throw new CardException("Too many cards in hand");
        }
        if (hands.contains(card)) {
            throw new CardException("duplicated card can't add");
        }
        hands.add(card);
    }

    /**
     * Provides the number of cards in hands.
     *
     * @return Number of cards
     */
    public int size() {
        return hands.size();
    }


    /**
     * Discards all the cards from hands.
     */
    public void discard() {
        if (hands.isEmpty()) {
            throw new CardException("hands is empty!");
        }
        hands.clear();
    }

    /**
     *  empties the hand of cards and
     *  returns each of them to the specified deck.
     * @param deck 1
     */
    public void discardTo(Deck deck) {
        if (hands.isEmpty()) {
            throw new CardException("hands is empty!");
        }
        for (Card card:hands) {
            deck.add(card);
        }
        hands.clear();
    }

    @Override
    public String toString() {
        List<String> handStr = new LinkedList<>();
        for (Card card:hands) {
            handStr.add(card.toString());
        }
        System.out.println(String.join(" ",handStr));
        return String.join(" ",handStr);
    }

    /**
     * just one pair
     *
     * @author bovane
     * @return boolean
     */
    public boolean isPair() {
        // poker num less than five
        if (hands.size()<5) {
            return false;
        }

        Map<String,Integer> countMap = new HashMap<>(5);
        for (Card card:hands) {
            String cardName = card.getRank().name();
            if (countMap.containsKey(cardName)) {
                // update the number of same key
                countMap.put(cardName,countMap.get(cardName)+1);
            }
            else {
                // add a new key-value
                countMap.put(cardName,1);
            }
        }

        // iter map,find pair
        int countPair = 0;

        for (Map.Entry<String,Integer> entry:countMap.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            if (entry.getValue()==2) {
                countPair+=1;
            }
        }
        if (countPair==1) {
            return true;
        }
        return false;
    }

    /**
     * two pairs
     *
     * @author bovane
     * @return boolean
     */
    public boolean isTwoPairs() {
        // poker num less than five
        if (hands.size()<5) {
            return false;
        }
        Map<String,Integer> countMap = new HashMap<>(5);

        // map cards rank to key value
        for (Card card:hands) {
            String cardName = card.getRank().name();
            if (countMap.containsKey(cardName)) {
                // update the number of same key
                countMap.put(cardName,countMap.get(cardName)+1);
            }
            else {
                // add a new key-value
                countMap.put(cardName,1);
            }
        }

        // iter count Map
        int countPair = 0;
        // int i
        for (Map.Entry<String,Integer> entry:countMap.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            // cards is pair
            if (entry.getValue()==2) {
                countPair+=1;
            }
        }

        if (countPair==2) {
            return true;
        }
        return false;
    }

    /**
     * detect three ACE
     *
     * @author bovane
     * @return boolean
     */
    public boolean isThreeOfAKind() {
        // poker num less than five
        if (hands.size()<5) {
            return false;
        }


        Map<String,Integer> countMap = new HashMap<>(5);
        // exclude  AAA22 case
        Boolean isPair = false;
        // count the number of each kind card
        for (Card card:hands) {
            // eg：rank is ACE，then cardName is "ACE"
            String cardName = card.getRank().name();
            if (countMap.containsKey(cardName)) {
                countMap.put(cardName,countMap.get(cardName)+1);
            }
            else {
                countMap.put(cardName,1);
            }
            // exclude  AAA22 case
            if (!cardName.equals("ACE") && countMap.get(cardName)==2) {
                isPair = true;
            }
        }


        // iter map to count ACE and exclude Pair
        if ( ( countMap.containsKey("ACE") && countMap.get("ACE") == 3) && !isPair) {
            return true;
        }
        return false;
    }

    /**
     * count four ACE
     *
     * @author bovane
     * @return boolean
     */
    public boolean isFourOfAKind() {
        // poker num less than five
        if (hands.size()<5) {
            return false;
        }
        Map<String,Integer> countMap = new HashMap<>(5);
        for (Card card:hands) {
            String cardName = card.getRank().name();
            if (countMap.containsKey(cardName)) {
                countMap.put(cardName,countMap.get(cardName)+1);
            }
            else {
                countMap.put(cardName,1);
            }
        }
        // iter map
        if (countMap.containsKey("ACE")&&countMap.get("ACE") == 4) {
            return true;
        }
        return false;
    }

    /**
     * detect full house
     *
     * @author bovane
     * @return boolean
     */
    public boolean isFullHouse() {
        // poker num less than five
        if (hands.size()<5) {
            return false;
        }
        Map<String,Integer> countMap = new HashMap<>(5);
        for (Card card:hands) {
            String cardName = card.getRank().name();
            if (countMap.containsKey(cardName)) {
                countMap.put(cardName,countMap.get(cardName)+1);
            }
            else {
                countMap.put(cardName,1);
            }
        }
        // iter map
        int countPair = 0;
        int countThree = 0;
        for (Map.Entry<String,Integer> entry:countMap.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            if (entry.getValue()==2) {
                countPair += 1;
            }
            else if (entry.getValue()==3) {
                countThree += 1;
            }
        }
        if ((countPair==1) && (countThree==1)) {
            return true;
        }
        return false;
    }

    /**
     * detect flush
     *
     * @author bovane
     * @return boolean
     */
    public boolean isFlush() {
        // poker num less than five
        if (hands.size()<5) {
            return false;
        }
        Map<String, Integer> countMap = new HashMap<>(5);
        for (Card card : hands) {
            String suitName = card.getSuit().name();
            if (countMap.containsKey(suitName)) {
                countMap.put(suitName, countMap.get(suitName) + 1);
            } else {
                countMap.put(suitName, 1);
            }
        }
        // iter map
        for (Map.Entry<String, Integer> entry : countMap.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            if (entry.getValue() == 5) {
                return true;
            }
        }
        return false;
    }

    /**
     * detect straight
     *
     * @author bovane
     * @return boolean
     */
    public boolean isStraight() {
        // poker num less than five
        if (hands.size()<5) {
            return false;
        }
        Map<String,Integer> countMap = new HashMap<>(5);
        for (Card card:hands) {
            String cardName = String.valueOf(card.getRank().getSymbol());
            if (countMap.containsKey(cardName)) {
                countMap.put(cardName,countMap.get(cardName)+1);
            }
            else {
                countMap.put(cardName,1);
            }
        }
        // the same poker less than five
        if (countMap.size()<5) {
            return false;
        }
        // transfer string key to Integer
        Set<String> keys=countMap.keySet();
        ArrayList<Integer> cardNum = new ArrayList<>(5);
        for(String s:keys) {
            if(s.charAt(0)>='2'&&s.charAt(0)<='9') {
                cardNum.add(Integer.valueOf(s));
            }else {
                switch(s.charAt(0)) {
                    case 'T':
                        cardNum.add(10);
                        break;
                    case 'J':
                        cardNum.add(11);
                        break;
                    case 'Q':
                        cardNum.add(12);
                        break;
                    case 'K':
                        cardNum.add(13);
                        break;
                    case 'A':
                        cardNum.add(1);
                        break;
                }
            }
        }
        int max = -1, min = 15;
        boolean isA = false;
        boolean isK = false;
        for(int num : cardNum) {
            max = Math.max(max, num);
            min = Math.min(min, num);
            if (num==1) {
                isA = true;
            }
            if (num==13) {
                isK = true;
            }
        }
        if (max - min < 5) {
            return true;
        }
        // deal with high ACE straight , TC JD QH KS AC
        if (isA && isK) {
            cardNum.remove(new Integer(1));
            cardNum.add(new Integer(14));
            // min max re-init
            max = -1;
            min = 15;
            for (int num: cardNum) {
                max = Math.max(max, num);
                min = Math.min(min, num);
            }
            if (max - min < 5) {
                return true;
            }
        }
        return false;
    }

}
